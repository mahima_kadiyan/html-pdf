
const pdf = require('html-pdf');
const getIsolationTemplate = (
  traveler,
  dateToday,
  travelerEmail,
) => {
  return `<!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8">
    <title>letterheaded paper</title>
    <style>
      * {
        box-sizing: border-box;
      }
      html {
        font-family: Arial;
      }
      body {
       font-size: smaller;
       margin: 20px 10px 0px 15px;
        background: rgb(255, 255, 255);
       }
       article {
         width: 150mm;
         height: 210mm;
         position: relative;
       }
       address {
         position: absolute;
         bottom: 70mm;
       }
       p {
       font-size: smaller;
        text-align: justify;
      }
      img {
        width: 250px;
      }
      h4 {
       font-weight: bold;
      }
      footer {
        font-size: smaller;
        bottom: 0mm;
      }
    </style>
  </head>
  <body>
    <article id="invoice">
      <h1><img
          src=''
          alt="State Department of Health"></h1>
      <p>${dateToday}</p>
      <h4><strong>Subject: Recommendation of Exclusion from Activities </strong> </h4>
      <p >To whom this may concern:<br><br>
        The State Department of Health and the ${traveler.county ? traveler.county : ''}
        County Health Department have conducted a public
        health investigation, and recommend
        ${traveler.firstName ? traveler.firstName : ''} ${traveler.lastName ? traveler.lastName : ''}
         be excluded from activities during the period of
        investigation. Once they have complied with our recommendations, and the investigation is complete, we will advise
        them that they may resume attendance. <br><br>
        We recognize the information provided to you and the reason for our exclusion recommendations is limited. We appreciate your 
        cooperation with our public health
        recommendations. <br><br>
        If you have any questions or concerns, please contact the Communicable Disease Nurse at your County Health
        Department, at <a
          href="#">Department Website</a>.<br><br>
      </p>
      <address>
        <p>Sincerely, </p>
          Commissioner of Health<br><br>
          <CC:ie>cc: ${traveler.firstName ? traveler.firstName : ''} ${traveler.lastName ? traveler.lastName : ''} </CC:ie><br>
          ${travelerEmail}
        </p>
      </address>
    </article>
    <footer>
    <a href="https://gov/health.html" >Health.gov</a></p>
  </footer>
  </body>
  </html>
  `;
};
isolationLetterGenerator = async () => {
    try {
        const traveler = {
            id: 1232,
            firstName: 'jake',
            lastName: 'smith',
            email: 'example@gmail.com',
            county: 'newCounty'
        };
        const dateToday = '12/20/2000';
        const travelerEmail = traveler.email ? traveler.email : '';
        const html = getIsolationTemplate(traveler, dateToday, travelerEmail);
        //provide bucket name here
        const bucketName = process.env.letterBucket
            ? process.env.letterBucket
            : '';
        let fileName = 'exclusionLetter';
        let file = {};
        //this is path for html-pdf to access phantomjs from
        const option = {
            phantomPath: './node_modules/phantomjs-prebuilt/bin/phantomjs',
        };
        pdf.create(html, option).toBuffer(async (err, buffer) => {
            if (err) return console.log(err);
            file = {
                originalname: fileName,
                buffer: buffer,
            };
            // replace uploadBuckerFile with your function to upload the file to GCS bucket now
            let uploadInfo = await uploadBucketFile(file, bucketName);
            return Promise.resolve({
                status: 200,
                message: 'Isolation letter has been generated successfully',
            });
        });
    } catch (e) {
        console.error('Error in Isolation letter generation', e);
        return Promise.reject(e);
    }
};
isolationLetterGenerator();


